import React, { useState } from 'react';
import { ModalProject } from '../services/ModalProject';
import projects from '../data/projects.json';
import './portfolio.css';

export const Portfolio = () => {
	const [ isOpen, setIsOpen ] = useState(false);
	const [ selectedProject, setSelectedProject ] = useState(1);

	const toggle = (selectedProject) => {
		setIsOpen(!isOpen);
	}

	const handleProject = (selectedProject) => {
		setSelectedProject(selectedProject);
		console.log("selectedProj = " + selectedProject);
		toggle(selectedProject);
	}

	return (
		<div>
			<h1>Mes Projets</h1>
			<div className='experienceContainer'>
				{projects.map((project, index) => {
					return (
						<div>
							<button className='projectCard' key={index} onClick={() => handleProject(project.id)}>
								<div className='cardTitle'> {project.intitule}</div>
								<div style= {{margin: '10px'}}>Objectif : </div>
								<div>{project.objectif}</div>
							</button>
							<ModalProject isOpen={isOpen} toggle={toggle} selectedProject={selectedProject} projects={projects} />
						</div>
					);
				})}
			</div>
		</div>
	);
};
