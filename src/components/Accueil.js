import React from 'react';

import './accueil.css';
import image from '../images/coding2.jpg';

export const Accueil = () => {
    return (
        <div>
            <div>
                <h1 className='title'>Cédric Chokbengboun</h1>
                <h5 className='subTitle'>Développeur</h5>
            </div>
            <div className='imageContainer'>
                <img className='displayed' src={image} alt="background"/>
            </div>
        </div>
    )
}