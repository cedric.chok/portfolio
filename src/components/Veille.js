import React from 'react';
import ImageGallery from 'react-image-gallery';
import './veille.css';
import feedly from '../images/Veille/feedly.PNG';
import google from '../images/Veille/googleActualites.PNG';
import pearltrees from '../images/Veille/pearltrees.PNG';

export const Veille = () => {
    const photos = [
        {
            original: `${feedly}`,
            description: "Feedly.com",
            thumbnail: `${feedly}`,
        },
        {
            original: `${google}`,
            description: "Google Actualités",
            thumbnail: `${feedly}`,
        },
        {
            original: `${pearltrees}`,
            description: "Pearltrees.com",
            thumbnail: `${feedly}`,
        },
    ];

    return (
        <div className="container">
            <div style={{fontWeight: 'bold'}}>Voici un petit topo sur un des sujet qui m'intéresse particulièrement... Le RGPD !</div>
            <div style={{marginTop: '10px'}}>Vous trouverez, dans cette partie, quelques informations à savoir sur le RGPD et des liens vers diverses sources.</div><br />
            <div style={{marginTop: '10px'}}>
                <div className="subTitle">
                    Qu'est-ce que le RGPD ?
                </div>
                <div style={{marginTop: '10px'}}>
                    Le Règlement Général sur la Protection des données (RGPD) est un texte législatif, applicable en mai 2018 dans toute l'Union Européenne, sur la protection des données. 
                    Le but étant de rendre les entreprises plus transparentes et d'étendre le respect de la vie privée des personnes concernées.
                </div>
            </div>
            <div>
                <div className="subTitle">
                    A qui s'applique-t-il ?
                </div>
                <div>
                    Il s'applique à toutes les entreprises collectant des données concernant les citoyens de l'UE ou celles dont les données sont stockées dans un des Etat membre.<br />
                    <br />
                    Source : <a href="https://www.lemagit.fr/definition/Reglement-general-sur-la-protection-des-donnees-RGPD">lemagit.fr</a>
                </div>
            </div>
            <div>
                <div className="subTitle">
                    Que doivent mettre en place les entreprises pour respecter le RGPD ?
                </div>
                <div>
                    Pour les entreprises gérant un flux important de données, il est recommandé de nominer un Data Protection Officier (DPO). 
                    Sa mission est de contrôler la conformité de l'entreprise au RGPD. <br />
                    <br />
                    <a href="https://www.e-marketing.fr/Thematique/insights-1092/Infographies/SnCD-DPO-2020-349222.htm">Pour en savoir plus sur le DPO</a> <br />
                    <br />
                    Les conseils de la CNIL : <br />
                    <li>Constituer un registre des traitements de données</li>
                    <li>Trier les données nécessaires et minimiser la collecte</li>
                    <li>Respecter le droit des personnes en matière de consultation, de rectification ou de suppression des données</li>
                    <li>Sécuriser les données</li><br />
                    Source : <a href="https://www.economie.gouv.fr/entreprises/reglement-general-sur-protection-des-donnees-rgpd">economie.gouv.fr</a><br />
                    <br />
                    <a href="https://www.cnil.fr/fr/rgpd-par-ou-commencer">Petit kit pratique de la CNIL ;-)</a><br />
                    <br />
                    <div>
                        Vous ne le saviez pas ? Il existe des logiciels qui proposent des solutions informatiques permettant d'automatiser la mise en conformité au RGPD. <br />
                        <a href="https://la-rem.eu/2020/01/des-logiciels-rgpd-pour-se-mettre-en-conformite-avec-le-reglement-europeen/">Quelques infos ici !</a>
                    </div>
                </div>
            </div>
            <div>
                <div className="subTitle">
                    La CNIL, le cyber-gendarme :
                </div>
                <div>
                    La Commission Nationale de l'Informatique et des Libertés (CNIL) est l'autorité administrative indépendante en charge de la protection des données personnelles en France. 
                    Elle a un rôle de conseiller, assistant et analyste dans la protection des données auprès des entreprises mais elle possède aussi un pouvoir de contrôle et de sanction 
                    administrative. <br />
                    <br/>
                    Il est difficile, même pour les plus grandes entreprises, de se conformer au RGPD.<br/>
                    <br/>
                    La CNIL sévit auprès de plusieurs grands noms. Elle n'hésite pas à mettre en demeure ou sanctionner financièrement pour manquement représentant une atteinte à la vie privée.<br/>
                    <br/>
                    En France : <br/>
                    <li>Critéo, entreprise numérique française </li>
                    <a href="https://www.usinenouvelle.com/article/criteo-dans-le-tourbillon-du-rgpd.N943926">usinenouvelle.com : Critéo dans le tourbillon du rgpd</a><br/>
                    <li>EDF et Engie sur les données Linky</li>
                    <a href="https://www.lebigdata.fr/rgpd-cnil-edf-engie-demeure">lebigdata.fr : EDF et Engie mis en demeure</a><br/>
                    <br/>
                    A l'étranger : <br/>
                    <li>Tinder et Google sous enquête</li>
                    <a href="https://www.numerama.com/politique/604159-rgpd-tinder-et-google-sont-sous-le-coup-dune-enquete-de-la-cnil-irlandaise.html">
                        numerama.com : Tinder et Google sous le coup d'une enquête de la cnil irlandaise
                    </a><br/>
                    <a href="https://www.lefigaro.fr/secteur/high-tech/2019/01/21/32001-20190121ARTFIG00199-rgpd-la-cnil-impose-a-google-une-sanction-record-de-50-millions-d-euros.php">
                        lefigaro.fr : La cnil impose à Google une sanction record de 50 millions d'euros
                    </a><br/>
                    <li>British Airways sanctionnée suite à un piratage informatique</li>
                    <a href="https://bfmbusiness.bfmtv.com/entreprise/rgpd-amende-record-de-200-millions-d-euros-pour-british-airways-1727301.html">
                        bfmbusiness.bfmtv.com : Amende record de 200 millions d'euros pour British Airways
                    </a><br/>
                    <br/>
                    Attention ! Etre conforme au rgpd n'est pas mission impossible. <br/>
                    <br/>
                    Le groupe SeLoger a fait une étude complète pour protéger ses données et en a fait un atout marketing !<br/>
                    <a href="https://www.lemagit.fr/etude/Groupe-SeLoger-quand-RGPD-rime-avec-agilite">
                        lemagit.fr : Groupe SeLoger quand RGPD rime avec agilité
                    </a><br/>
                    <br/>
                    Ou encore Commanders Act, éditeur de logiciel et leader européen du tag et de la data, a obtenu le Label d'excellence de la plateforme européenne RGPD Check. <br/>
                    <a href="https://www.globalsecuritymag.fr/Commanders-Act-obtient-le-Label,20200526,99001.html">
                    globalsecuritymag.fr : Commanders Act obtient le label Excellence RGPD Check 
                    </a><br/>
                </div>
            </div>
            <div>
                <div className="subTitle">
                    Où en sommes-nous aujourd'hui ?
                </div>
                <div>
                    Après 2 années d'application, seul un site sur dix respecterait les règles du RGPD liées au consentement aux cookies.<br/>
                    <a href="https://www.usine-digitale.fr/article/seul-un-site-sur-dix-respecterait-les-regles-du-rgpd-liees-au-consentement-aux-cookies.N919239">
                        usine-digitale.fr
                    </a><br/>
                    <br/>
                    Malgré des millions d'euros dépensés pour se conformer au RGPD, de nombreuses entreprises restent vulnérables. Et ce n'est pas faute d'y mettre les moyens :<br/>
                    <li>Recrutement de talents dans la sécurité informatique</li>
                    <li>Investissement dans la formation</li>
                    <li>Introduction de nouveaux logiciels ou services</li><br/>
                    La structure de l'entreprise ou le manque de moyen sont les principaux freins à cette mise en conformité.<br/>
                    <a href="https://www.globalsecuritymag.fr/Conformite-au-RGPD-un-defi,20200525,98946.html">
                        globalsecuritymag.fr : Conformité au RGPD un défi
                    </a>
                </div>
            </div>
            <div>
                <div className="subTitle">
                    Et les développeurs dans tout ça ?
                </div>
                <div>
                    Il est de son devoir pour une entreprise de mettre les moyens afin de protéger les données de ses salariés, utilisateurs et clients. 
                    Mais, nous développeurs, avons également une part de responsabilité. Notre travail ayant pour objectif de produire des solutions informatiques traitant des données, 
                    il est important de prendre des précautions. <br/>
                    <br/>
                    Voici donc un petit bonus ;-) <br/>
                    <a href="https://www.cnil.fr/fr/guide-rgpd-du-developpeur">Guide rgpd du géveloppeur !!</a><br/>
                    <br/>
                </div>
            </div>
            <div>
                <div className="subTitle">Mes sources : </div>
                <div>
                    <ImageGallery items={photos} />
                </div>
            </div>
        </div>
    )
}