import React from 'react';
import './aboutMe.css';

export const AboutMe = () => {
    return (
        <div className='aboutMeContainer'>
            <p style= {{fontSize: '30px'}}>Bienvenue sur mon portfolio !</p>
            
            <p>Moi, c'est cédric. J'ai 34 ans et en pleine reconversion professionnelle. De nature calme, j'adore passer mes journées en famille, voyager, cuisiner, pêcher ou jouer à la pétanque.</p>

            <p>Fort d'un parcours à caractère commercial et managérial, j'ai plus de 10 ans d'expérience professionnelle à mon actif.</p>

            <p>Durant ma carrière, j'ai accompli de nombreuses missions, dans de nombreuses villes et même à l'international. J'ai décidé, voilà 2 ans maintenant, 
            de me lancer un défi de taille : Devenir développeur !</p> 

            <p style= {{fontSize: '30px'}}>Pourquoi changer de métier ?</p>

            <p>J'adorais mon métier de manager pour lequel j'ai toujours donné le meilleur de moi-même. Mon travail m'a permis de recontrer de merveilleuses personnes
                auprès desquelles j'ai énormément appris. A mon tour, j'ai pu en aider d'autres en leur transmettant mon savoir-faire, ma volonté de réussir et ma motivation.
                Après plusieurs années dans le même domaine, l'idée de changer de métier trottait dans ma tête. J'étais fier de mon travail et de ce que j'avais accompli mais j'avais l'impression de 
                stagner. Etant un grand challenger, je pensais que je pouvais sortir de ma zone de confort et réussir dans d'autres domaines. L'annonce de la naissance ma fille a été
                l'élément déclencheur. C'est à ce moment que j'ai décidé de faire le grand saut !
            </p>

            <p style= {{fontSize: '30px'}}>Pourquoi devenir développeur ?</p>
            
            <p>Alors oui, j'avais décidé de changer. Mais pour quoi faire ? Un défi simple penseront certains... Mais en fait non ! Repartir de zéro et trouver un métier passionnant correspondant
                à ma personnalité n'était pas si facile. Pendant plus d'un an, j'ai essayé la restauration non franchisée, la banque, la vente et plein d'autres. Malheureusement, je n'y trouvais aucune 
                affinité. J'étais troublé... Je ne savais pas ce qu'il me plairait. J'étais en recherche d'inspiration. J'observais partout, je lisais toutes sortes d'articles sur les investisseurs, 
                les métiers les plus demandés etc...              
            </p>

            <p>Puis un jour, j'ai repris contact avec un ancien camarade de classe de commerce international. Lui, avait décidé de tout quitter pour devenir développeur.
                Intrigué, je lui ai demandé en quoi cela consistait. Il m'a alors expliqué que développer n'était pas de taper à l'écran des milliers de lignes de code incompréhensibles. 
                Derrière ces lignes, il y avait des algorithmes, de la logique et beaucoup de réflexion. Que pour devenir développeur, il ne fallait jamais rester sur ses acquis et toujours devoir 
                mettre à jour ses connaissances. Plusieurs notions que je mettais déjà en application en tant que manager. Sauf que dans les nouvelles technologies, tout évolue encore plus vite ! 
                Donc pas le temps de s'ennuyer ! 
            </p>

            <p>Me voilà donc en route pour devenir développeur. Choix que je ne regrette pas. Chaque journée est pleine d'excitation au travers de mes différents projets. 
                Aujourd'hui, je me réveille en pensant code, je passe mes journées en pensant code et je m'endors en pensant encore code. Coder est devenu une nouvelle passion ! </p>

            <p>
                Maintenant que vous me connaissez un peu mieux, et si vous souhaitez en savoir davantage sur moi, je vous invite à visiter mes autres rubriques.
            </p>
        </div>
    )
}