import React, { useState } from 'react';
import { ModalExperience } from '../services/ModalExperience';
import experiences from '../data/experiences.json';
import './experiences.css';

import aston from '../logos/aston_Ecole.png';
import greta from '../logos/logo_greta.jpg';
import ens from '../logos/ecole_normale_superieure.jpg';
import snecma from '../logos/Snecma_logo.jpg';
import sg from '../logos/societe-generale-logo.jpg';
import dominos from '../logos/dominos_logo.png';
import canada from '../logos/logo_canada.svg.png';


export const Experiences = () => {

    const [isOpen, setIsOpen] = useState(false);
    const [selectedExperience, setSelectedExperience] = useState(1);

    const toggle = (selectedExperience) => {
        setIsOpen(!isOpen);
    }

    const handleExperience = (selectedExperience) => {
        setSelectedExperience(selectedExperience);
        console.log("selectedExp = " + selectedExperience);
        toggle(selectedExperience);
    }

    return (
        <div>
            <h1 className='title'>Mes expériences professionnelles</h1>
            
            <div className='experienceContainer'>
            { experiences.map((experience, index) => {
                    return(
                        <>
                            <button onClick={() => handleExperience(experience.id)} className='experienceCard' key={index}>
                                <div className='experienceTitle'>{experience.intitule}</div>
                                <div>{experience.entreprise}</div>
                                <div>{experience.date}</div>
                                <div className='logoStyle'>
                                    {experience.entreprise === 'Aston Ecole' ? 
                                    <img className='logoDisplayed' src={aston} alt="logo" /> : 
                                    experience.entreprise === 'Greta des Yvelines' ?
                                    <img className='logoDisplayed' src={greta} alt="logo" /> :
                                    experience.entreprise === 'Ecole Normale Supérieure' ?
                                    <img className='logoDisplayed' src={ens} alt="logo" /> :
                                    experience.entreprise === 'Dominos Pizza' ?
                                    <img className='logoDisplayed' src={dominos} alt="logo" /> :
                                    experience.entreprise === 'Société Générale' ?
                                    <img className='logoDisplayed' src={sg} alt="logo" /> :
                                    experience.entreprise === 'SNECMA Groupe Safran' ?
                                    <img className='logoDisplayed' src={snecma} alt="logo" /> :
                                    experience.entreprise === 'Ban Thai Montréal' ?
                                    <img className='logoDisplayed' src={canada} alt="logo" /> :
                                    null}
                                </div>
                            </button>
                            <ModalExperience isOpen={isOpen} toggle={toggle} selectedExperience ={selectedExperience} experiences={experiences} />
                        </>
                    );
                })}
            </div>
        </div>
    )
}