import React, { useState } from 'react';
import './home.css';
import { Accueil } from './Accueil';
import { AboutMe } from './AboutMe';
import { Formations } from './Formations';
import { Experiences } from './Experiences';
import { Portfolio } from './Portfolio';
import { Veille } from './Veille';

export const Home = () => {
    const [selectedButton, setSelectedButton] = useState('accueil');

    

    const handleClick = (event) => {
        setSelectedButton(event.currentTarget.value);
    }

    // const handleCategory = (selectedButton) => {
        
    //         switch (selectedButton) {
    //             case 'accueil':
    //                 return <Accueil />
    //             case 'presentation':
    //                 return <AboutMe />
    //             case 'formations':
    //                 return <Formations />
    //             case 'experiences':
    //                 return <Experiences />
    //             case 'portfolio':
    //                 return <Portfolio />
    //             default:
    //                 return <Home />
    //         }
        
    // }

    return (
        <div>
            <div className='headerContainer'>
                <button value='accueil' onClick={handleClick} className='navButton'>Accueil</button>
                <button value='presentation' onClick={handleClick} className='navButton'>Présentation</button>
                <button value='formations' onClick={handleClick} className='navButton'>Formations</button>
                <button value='experiences' onClick={handleClick} className='navButton'>Expériences</button>
                <button value='portfolio' onClick={handleClick} className='navButton'>Projets</button>
                <button value='rgpd' onClick={handleClick} className='navButton'>RGPD</button>
            </div>
            <div className='mainContainer'>
                {/* {() => handleCategory(selectedButton)} */}
                
                {selectedButton === 'accueil' ? 
                    <Accueil /> : (null)
                }
                {selectedButton === 'presentation' ? 
                    <AboutMe /> : (null)
                }
                {selectedButton === 'formations' ? 
                    <Formations /> : (null)
                }
                {selectedButton === 'experiences' ? 
                    <Experiences /> : (null)
                }
                {selectedButton === 'portfolio' ? 
                    <Portfolio /> : (null)
                }
                {selectedButton === 'rgpd' ? 
                    <Veille /> : (null)
                }
            </div>
            {/* <div className='footerContainer'>Pied de page</div> */}
        </div>
    )
}