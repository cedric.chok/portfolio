import React from 'react';
import formationsData from '../data/formations.json';

import './formations.css';

export const Formations = () => {

    return (
        <div>
            <h1 className='title'>Mes formations</h1>
            {formationsData.map((formation, index) => {
                return (
                    <div key={index}>
                        <div className='formationBanner'>
                            <div className='formationTitle'>{formation.diplome}</div>
                            <div>{formation.etablissement}, {formation.ville}</div>
                            <div>Année : {formation.annee}</div>
                        </div>
                    </div>
                );
            })}
        </div>
    )
}