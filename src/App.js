import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import './App.css';
import { Home } from '../src/components/Home';
import { AboutMe } from './components/AboutMe';
import { Formations } from './components/Formations';
import { Experiences } from './components/Experiences';
import { Portfolio } from './components/Portfolio';

function App() {
  return (
    <BrowserRouter>
      <Route exact path='/' component={ Home } />
      <Route exact path='/aboutme' component={ AboutMe } />
      <Route exact path='/myformations' component={ Formations } />
      <Route exact path='/experiences' component={ Experiences } />
      <Route exact path='/myprojects' component={ Portfolio } />
    </BrowserRouter>
  );
}

export default App;
