import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import './modalExperience.css';

export const ModalExperience = (props) => {
	const {
		isOpen,
		className,
		toggle,
		selectedExperience,
		experiences
	} = props;

	  const exp = experiences.find(element => element.id === selectedExperience);

	return (
		<div>
			{/* <Button color="danger" onClick={toggle}>{buttonLabel}</Button> */}
			<Modal isOpen={isOpen} toggle={() => toggle(selectedExperience)} className={className}>
				<ModalHeader className="header" toggle={toggle}>{exp.intitule}</ModalHeader>
				<ModalBody>
					<div style={{ fontWeight: 'bold' }}>Type de contrat : {exp.statut}</div>
					Les missions réalisées :
					{exp.missions.map((mission, index) => {
						return (
							<ul>
								<li key={index}>{mission}</li>
							</ul>
						);
					})}
				</ModalBody>
				<ModalFooter>
					<Button color="secondary" onClick={toggle}>
						Fermer
					</Button>
				</ModalFooter>
			</Modal>
		</div>
	);
};
