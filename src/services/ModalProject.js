import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import ImageGallery from 'react-image-gallery';

import './modalProject.css';

import js from '../logos/logo_JS.png';
import html from '../logos/logo_html.jpg';
import css from '../logos/logo_css.jpg';
import react from '../logos/logo_react.jpg';
import vsCode from '../logos/logo_visualStudioCode.jpg';
import teams from '../logos/logo_teams.jpg';
import jira from '../logos/logo_jira.jpg';
import php from '../logos/logo_php.jpg';
import netbeans from '../logos/logo_netbeans.png';
import bootstrap from '../logos/logo_bootstrap.png';
import sql from '../logos/logo_sql.jpg';
import java from '../logos/logo_java.jpg';
import swing from '../logos/logo_swing.jpg';
import eclipse from '../logos/logo_eclipse.jpg';
import typescript from '../logos/logo_typescript.png';
import cSharp from '../logos/logo_cSharp.jpg';
import angular from '../logos/logo_angular.png';
import vs from '../logos/logo_visualStudio.jpg';
import azure from '../logos/logo_azureDevops.jpg';
import sqlServer from '../logos/logo_sql_server.jpg';
import nodeJs from '../logos/logo_nodeJS.jpg';
import postgre from '../logos/logo_postgre.jpg';

import m2l_1 from '../images/M2L_Captures/m2l_login2.PNG';
import m2l_2 from '../images/M2L_Captures/m2l_inscription1.PNG';
import m2l_3 from '../images/M2L_Captures/m2l_inscription2.PNG';
import m2l_4 from '../images/M2L_Captures/m2l_accueil.PNG';
import m2l_5 from '../images/M2L_Captures/m2l_formations.PNG';
import m2l_6 from '../images/M2L_Captures/m2l_inscription_session.PNG';
import m2l_7 from '../images/M2L_Captures/m2l_mesFormations.PNG';
import m2l_8 from '../images/M2L_Captures/m2l_modif_profil1.PNG';
import m2l_9 from '../images/M2L_Captures/m2l_modif_profil2.PNG';

import azureSprint from '../images/AstonProject/AzureDevops.PNG';
import azureTask from '../images/AstonProject/AzureDevops_Tache.PNG';
import accueil from '../images/AstonProject/Gaston_Accueil.PNG';
import evaluation from '../images/AstonProject/Evaluation.PNG';
import diplome from '../images/AstonProject/Gaston_Diplomes.PNG';
import diplome2 from '../images/AstonProject/Gaston_Diplomes_Modif_Eleve.PNG';
import certificateur from '../images/AstonProject/Gaston_Certificateur.PNG';
import typeTitre from '../images/AstonProject/Gaston_TypeTitre.PNG';
import emargement from '../images/AstonProject/Gaston_Emargement.PNG';
import emargementXls from '../images/AstonProject/Gaston_Emargement_Xls_Avant.PNG';
import emargementXls2 from '../images/AstonProject/Gaston_Emargement_Xls_Apres.PNG';
import test from '../images/AstonProject/Gaston_Plan_Test.PNG';
import docTechnique from '../images/AstonProject/Redac_Doc_Technique.PNG';

export const ModalProject = (props) => {
	const { isOpen, className, toggle, selectedProject, projects } = props;

	const proj = projects.find((element) => element.id === selectedProject);

	const m2l_pictures = [
		{
			original: `${m2l_1}`,
			description: 'Authentification',
			thumbnail: `${m2l_1}`
		},
		{
			original: `${m2l_2}`,
			description: 'Page de création de compte',
			thumbnail: `${m2l_2}`
		},
		{
			original: `${m2l_3}`,
			description: 'Page de création de compte avec menu déroulant',
			thumbnail: `${m2l_3}`
		},
		{
			original: `${m2l_4}`,
			description: "Page d'accueil",
			thumbnail: `${m2l_4}`
		},
		{
			original: `${m2l_5}`,
			description: 'Page des formations disponibles',
			thumbnail: `${m2l_5}`
		},
		{
			original: `${m2l_6}`,
			description: 'Page des sessions disponibles sur la formation précédemment sélectionnée',
			thumbnail: `${m2l_6}`
		},
		{
			original: `${m2l_7}`,
			description: "Page des formations auxquelles l'utilisateur s'est inscrit",
			thumbnail: `${m2l_7}`
		},
		{
			original: `${m2l_8}`,
			description: "Page de profil de l'utilisateur authentifié",
			thumbnail: `${m2l_8}`
		},
		{
			original: `${m2l_9}`,
			description: 'Page de modification du profil',
			thumbnail: `${m2l_9}`
		}
	];

	const aston_pictures = [
		{
			original: `${azureSprint}`,
			description: 'Azure Devops est un outil de plannification de sprints, méthode agile Scrum',
			thumbnail: `${azureSprint}`
		},
		{
			original: `${azureTask}`,
			description: 'Azure Devops : Description de tâche',
			thumbnail: `${azureTask}`
		},
		{
			original: `${accueil}`,
			description: "Page d'accueil de l'outil de gestion administratif Gaston",
			thumbnail: `${accueil}`
		},
		{
			original: `${evaluation}`,
			description: "Gaston : Revue de code, Page d'évaluation des élèves",
			thumbnail: `${evaluation}`
		},
		{
			original: `${diplome}`,
			description: "Gaston : Développement d'interface graphique , page de gestion des diplômes",
			thumbnail: `${diplome}`
		},
		{
			original: `${diplome2}`,
			description: "Gaston : Développement d'interface graphique , page de gestion des diplômes",
			thumbnail: `${diplome2}`
		},
		{
			original: `${certificateur}`,
			description: "Gaston : Développement d'interface graphique , page de gestion des certificateurs",
			thumbnail: `${certificateur}`
		},
		{
			original: `${typeTitre}`,
			description: "Gaston : Développement d'interface graphique , page de gestion des types de diplôme",
			thumbnail: `${typeTitre}`
		},
		{
			original: `${emargement}`,
			description: "Gaston : Correction de code, page d'édition des feuilles d'émargement",
			thumbnail: `${emargement}`
		},
		{
			original: `${emargementXls}`,
			description: "Gaston : Correction de code, Génération de feuille d'émargement avant",
			thumbnail: `${emargementXls}`
		},
		{
			original: `${emargementXls2}`,
			description: "Gaston : Correction de code, Génération de feuille d'émargement après",
			thumbnail: `${emargementXls2}`
		},
		{
			original: `${test}`,
			description: 'Test fonctionnel effectué avant mise en production',
			thumbnail: `${test}`
		},
		{
			original: `${docTechnique}`,
			description: "Rédaction d'une documentation technique pour les utilisateurs",
			thumbnail: `${docTechnique}`
		}
	];

	return (
		<div>
			{/* <Button color="danger" onClick={toggle}>{buttonLabel}</Button> */}
			<Modal isOpen={isOpen} toggle={() => toggle(selectedProject)} className={className}>
				<ModalHeader className="header" toggle={toggle}>
					{proj.intitule}
				</ModalHeader>
				<ModalBody>
					<div>
						<div>
							<div className="modalTitle">Les missions réalisées : </div>
							{proj.missions.map((mission, index) => {
								return (
									<ul>
										<li key={index}>{mission}</li>
									</ul>
								);
							})}
						</div>
						<div>
							<div className="modalTitle">Langages utilisés : </div>
							<div className="logoContainer">
								{proj.langages.map((langage, l) => {
									return (
										<div key={l}>
											{langage === 'javascript' ? (
												<img className="logoDisplay" src={js} alt={langage} />
											) : langage === 'html' ? (
												<img className="logoDisplay" src={html} alt={langage} />
											) : langage === 'css' ? (
												<img className="logoDisplay" src={css} alt={langage} />
											) : langage === 'php' ? (
												<img className="logoDisplay" src={php} alt={langage} />
											) : langage === 'java' ? (
												<img className="logoDisplay" src={java} alt={langage} />
											) : langage === 'typescript' ? (
												<img className="logoDisplay" src={typescript} alt={langage} />
											) : langage === 'cSharp' ? (
												<img className="logoDisplay" src={cSharp} alt={langage} />
											) : langage === 'nodeJS' ? (
												<img className="logoDisplay" src={nodeJs} alt={langage} />
											) : langage === 'sql' ? (
												<img className="logoDisplay" src={sql} alt={langage} />
											) : null}
										</div>
									);
								})}
							</div>
						</div>
						<div>
							<div className="modalTitle">Framework(s) utilisé(s) : </div>
							<div className="logoContainer">
								{proj.frameworks.map((framework, f) => {
									return (
										<div key={f}>
											{framework === 'react' ? (
												<img className="logoDisplay" src={react} alt={framework} />
											) : framework === 'bootstrap' ? (
												<img className="logoDisplay" src={bootstrap} alt={framework} />
											) : framework === 'swing' ? (
												<img className="logoDisplay" src={swing} alt={framework} />
											) : framework === 'angular' ? (
												<img className="logoDisplay" src={angular} alt={framework} />
											) : null}
										</div>
									);
								})}
							</div>
						</div>
						<div>
							<div className="modalTitle">Outil(s) utilisé(s) : </div>
							<div className="logoContainer">
								{proj.outils.map((outil, o) => {
									return (
										<div key={o}>
											{outil === 'visual studio code' ? (
												<img className="logoDisplay" src={vsCode} alt={outil} />
											) : outil === 'teams' ? (
												<img className="logoDisplay" src={teams} alt={outil} />
											) : outil === 'jira' ? (
												<img className="logoDisplay" src={jira} alt={outil} />
											) : outil === 'netbeans' ? (
												<img className="logoDisplay" src={netbeans} alt={outil} />
											) : outil === 'eclipse' ? (
												<img className="logoDisplay" src={eclipse} alt={outil} />
											) : outil === 'visual studio' ? (
												<img className="logoDisplay" src={vs} alt={outil} />
											) : outil === 'azureDevops' ? (
												<img className="logoDisplay" src={azure} alt={outil} />
											) : outil === 'sqlServer' ? (
												<img className="logoDisplay" src={sqlServer} alt={outil} />
											) : outil === 'postgre' ? (
												<img className="logoDisplay" src={postgre} alt={outil} />
											) : null}
										</div>
									);
								})}
							</div>
						</div>
						<div>
							<div className="modalTitle">Galerie d'images</div>
							<div className="galleryContainer">
								{proj.intitule === "Site PHP, gestion d'inscriptions" ? (
									<ImageGallery items={m2l_pictures} />
								) : proj.intitule === 'Application Web C#, Angular' ? (
									<ImageGallery items={aston_pictures} />
								) : null}
							</div>
						</div>
					</div>
				</ModalBody>
				<ModalFooter>
					<Button color="secondary" onClick={toggle}>
						Fermer
					</Button>
				</ModalFooter>
			</Modal>
		</div>
	);
};
